$(document).ready(function () {



    // expande artigo

    $('.btn-expand').click(function () {
        $(this).closest('.item-article').find('.hidden-content').toggleClass('show-content');
        $(this).toggleClass('active');
    });


    $('.btn-expand-filter').click(function () {
        $(this).toggleClass('active');
        $('.map-filters').toggleClass('active');
    });

    $('.btn-acervo-filters').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active');

    });

    // scroll



    $('.btn-up-scroll').click(function () {
        $('.box-scroll').scrollTop($('.box-scroll').scrollTop()-20);
    });

    $('.btn-down-scroll').click(function () {
        $('.box-scroll').scrollTop($('.box-scroll').scrollTop()+20);
    });

    $('.zoom-btn').click(function (e) {
        e.preventDefault();
        var ativo = $('#sliderinterno').find('.active a');
        //console.log(ativo);
        ativo.trigger('click');
    });


    $('.mobshowfilter').click(function () {
        $('.map-options').toggleClass('active');
    });

    $('.btn-mobile-button').click(function () {
        $(this).toggleClass('active');
        $('.btn-mobile-button').not($(this)).removeClass('active');
    });


    $('.menu-lateral-obras ul li a').click(function () {
        $(this).toggleClass('active');
        $('.menu-lateral-obras ul li a').not($(this)).removeClass('active');
    });



    // scroll page up
    $('.btn-up-scroll-full').click(function () {
        $('html, body').animate({ scrollTop: 0 }, 600);
        return false;
    })

    $(document).scroll(function() {
        var y = $(this).scrollTop();
        if (y > 500) {
            $('.btn-up-scroll-full').fadeIn();
        } else {
            $('.btn-up-scroll-full').fadeOut();
        }
    });


    // historia
    $('#fixscroll').scrollToFixed({
        top: 0,
        limit: $(document).height() - $('footer').height() /*- $(window).height()*/ - 630,
    });

    //console.log($(document).height());
    //console.log($(document).height() + 150);
    //console.log($(document).height() -  $('footer').height());


    // HISTORIA SCROLL


    // Cache selectors
    var lastId,
        topMenu = $('#selected'),
        topMenuHeight = topMenu.outerHeight()+15,
        // All list items
        menuItems = topMenu.find('a'),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
            var item = $($(this).attr('href'));
            if (item.length) { return item; }
        });

// Bind click handler to menu items
// so we can get a fancy scroll animation
    menuItems.click(function(e){
        var href = $(this).attr('href'),
            offsetTop = href === '#' ? 0 : $(href).offset().top-topMenuHeight+1;
        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, 300);
        e.preventDefault();
    });

// Bind to scroll
    $(window).scroll(function(){
        // Get container scroll position
        var fromTop = $(this).scrollTop()+topMenuHeight + 400; // <- posicao do elemento final na página + 400

        // Get id of current scroll item
        var cur = scrollItems.map(function(){
            if ($(this).offset().top < fromTop)
                return this;
        });
        // Get the id of the current element
        cur = cur[cur.length-1];
        var id = cur && cur.length ? cur[0].id : '';

        if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            menuItems
                .parent().removeClass('active')
                .end().filter('[href=\'#'+id+'\']').parent().addClass('active');
        }
    });



    // FIM HISTORIA SCROLL




    $('.btn-up-scroll-date').click(function () {
        $('.timeline').scrollTop($('.timeline').scrollTop()-20);
    });

    $('.btn-down-scroll-date').click(function () {
        $('.timeline').scrollTop($('.timeline').scrollTop()+20);
    });

    $('.ico-xingu').click(function () {
        $(this).toggleClass('active')
    });



    // CORREDOR

    $('.content-mapa-corredor').click(function(e) {

        e.preventDefault();
        var parentOffset = $(this).parent().offset();
        var relX = e.pageX - parentOffset.left - 17;
        var relY = e.pageY - parentOffset.top;
        /*var img = $('<img>');
        img.css('top', relY);
        img.css('left', relX);
        img.attr('src', '../images/mapa/pin.png');
        img.appendTo('.content-mapa-corredor');*/
        console.log('X e Y', relX +'/'+ relY);
        //console.log('relY', relY);

    });


    var infos = [{
        'id' : '1',
        'nome': 'Belo Monte',
        'titulo': 'Lorem Ipsum Dollor',
        'desc': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet lorem sed risus tincidunt vestibulum eu vel lectus.',
        'position':
            {
                'x' : '95.5',
                'y' : '274',
            },
        'infos' : {
            'povos' : '2',
            'populacao' : '31232',
            'area' : '456mil',
            'news' : '600',
        }

        },
        {
            'id' : '2',
            'nome': 'Teste 2',
            'titulo': 'Lorem',
            'desc': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet lorem sed risus tincidunt vestibulum eu vel lectus.',
            'position':
                {
                    'x' : '101',
                    'y' : '500',
                },
            'infos' : {
                'povos' : '4',
                'populacao' : '1212',
                'area' : '240mil',
                'news' : '200',
            }

        }
    ]


    $('.list-map-corredor').html(function () {

        var len = infos.length
        for(var i = 0; i<len; i++)
        {
            //console.log(infos[i]);
            $('#mobilecorredor').append('<option value="'+infos[i].id+'">'+infos[i].nome+'</option>')
            $('.list-map-corredor').append('<li id="list'+infos[i].id+'" data-item="'+infos[i].id+'" class="mapitemlist"><a href="#listconent'+infos[i].id+'"  >'+infos[i].nome+'</a></li>')
            var img = $('<img data-item="'+infos[i].id+'" class="maptriangle" id="icomap'+infos[i].id+'">');
            img.attr('src', 'http://nelsoncampos.com.br/xingu/images/mapa/pin.png');
            img.appendTo('.content-mapa-corredor');
            img.css('top', infos[i].position.y + 'px');
            img.css('left', infos[i].position.x + 'px');
            if (i == 0){
                $('#list'+infos[i].id+'').addClass('active');
            }
        }

    });

   onLoad(1);
   $('.mapitemlist').click((search));
   $('.maptriangle').click((search));

    // atualiza item mostrado
    function search() {

        var current = $(this).data('item');
        //console.log('current', current);

        var result = $.grep(infos, function(e){ return e.id == current; });
        var itemlen = result.length;

        for (var i = 0; i<itemlen; i++){
            $('.project-title').html(result[i].nome);
            $('.project-description').html(result[i].desc);
            //$('.povos').html(result[i].infos.povos);
            $('.populacao').html(result[i].infos.populacao);
            $('.area').html(result[i].infos.area);
            $('.news').html(result[i].infos.news);
        }

        $('.maptriangle').attr('src', 'http://nelsoncampos.com.br/xingu/images/mapa/pin.png');
        $('.mapitemlist').removeClass('active');
        $('#icomap'+current+'').attr('src', 'http://nelsoncampos.com.br/xingu/images/mapa/pin-active.png');
        $('#list'+current+'').addClass('active');
    }

    function onLoad(item) {
        var current = item;
        var result = $.grep(infos, function(e){ return e.id == current; });
        var itemlen = result.length;

        for (var i = 0; i<itemlen; i++){
            $('.project-title').html(result[i].nome);
            $('.project-description').html(result[i].desc);
            //$('.povos').html(result[i].infos.povos);
            $('.populacao').html(result[i].infos.populacao);
            $('.area').html(result[i].infos.area);
            $('.news').html(result[i].infos.news);
            $('#icomap'+current+'').attr('src', 'http://nelsoncampos.com.br/xingu/images/mapa/pin-active.png');
            $('#list'+current+'').addClass('active');
        }
    }

    $('#mobilecorredor').on('change', function() {
        onLoad( this.value );
    })

    $('#maisfilters').select2({
        placeholder: 'Escolha os filtros clicando aqui'
    });


    // atualizacoes 13/10

    $('.sidebar-interna.obras').scrollToFixed(({ marginTop: 10, limit: $('footer').offset().top}));
    $('.sidebar-interna.parceiros').scrollToFixed(({ marginTop: 10, limit: $('.parthnerslogo').offset().top - $('.sidebar-interna.parceiros').outerHeight(true) - 10}));

})

