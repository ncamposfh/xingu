# XINGU #

Desenvolvido por Nelson Campos (nelsoncf.com) 

### Como começar a desenvolver ###

* Tenha NodeJs instalado na sua máquina, além de gulp e npm 
* Digite os comandos abaixo no seu terminal, veja se você está na pasta do projeto e execute-os:


```
seu-usuario$ npm install
```
O comando acima instala as dependências do projeto, o comando abaixo instala as dependências front end.

```

seu-usuario$ bower install
```

### Tasks do Gulp (principais, você só vai usar essas) ###

* gulp serve -> Inicial o servidor local para trabalhar, ele executa na porta 9000 (localhost:9000)
* gulp build -> gera o arquivo final (site em produção), joga os arquivos finais dentro da pasta DIST. Tudo concatenado e minificado.

### Está com duvida? ###

Pra explicar melhor o que está acontecendo, eu usei o [Yeoman Webapp Generator (documentação aqui)](https://github.com/yeoman/generator-webapp), ele usa gulp para automatização de tarefas.
O projeto usa SASS como pre-procesador de CSS, Modernizer e Bootstrap na estrutura. Não foi usado nenhum codficador de JS, o JS do site é puro.

### Como você fala comigo? ###

* Celular (Whatsapp): +55 11 98600 7636
* Email: nelson@nelsoncf.com / ncamposfh@gmail.com
